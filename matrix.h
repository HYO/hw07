#ifndef __MATRIX__
#define __MATRIX__
#include<vector>
#include<iostream>

using namespace std;

class Matrix {

 public:

	 Matrix() {Resize(0,0);}

	 Matrix(const Matrix& m) { 
		 rows_=m.rows();
		 cols_=m.cols(); 
		 Resize(rows_,cols_);
		 for(int i=0; i<rows_; i++)
			 for(int j=0; j<cols_; j++) 
				 values_[Sub2Ind(i,j)]=m(i,j);
	 }

	 Matrix(int rows, int cols) { rows_=rows; cols_=cols; }


  int rows() const { return rows_; }

  int cols() const { return cols_; }

  void Resize(int rows, int cols) {
    
    values_.resize((rows_ = rows) * (cols_ = cols));

  }

  int& operator()(int r, int c) {return values_[Sub2Ind(r,c)];}

  const int& operator()(int r, int c) const {return values_[Sub2Ind(r,c)];}


  Matrix operator+() const; 
  Matrix operator-() const; 
  Matrix Transpose() const;  


 private:


  int Sub2Ind(int r, int c) const { return r + c * rows_; }

  std::vector <int> values_;  

  int rows_, cols_;   


};


class Vector : public Matrix {

 public:

	 Vector() { Matrix(); }

	 Vector(const Vector& v) {
		 int rows=v.rows();
		 Matrix::Resize(rows,1);
		 for(int i=0; i<rows; i++)
				Matrix::operator()(i,0)=v(i); 
	 }

	 Vector(int rows) { Matrix(rows,1); }

	 int rows() const{ return Matrix::rows();}

	 int cols() const{ return 1; }

	 void Resize(int rows) { Matrix::Resize(rows,1); }

  int& operator[](int r)  {return Matrix::operator()(r,0);}

  const int& operator()(int r) const{return  Matrix::operator()(r,0);}


};

istream& operator>>(istream& is, Matrix& m);


istream& operator>>(istream& is, Vector& v) ;


ostream& operator<<(ostream& os, const Matrix& m) ;

Matrix operator+(const Matrix& lm, const Matrix& rm);

Matrix operator-(const Matrix& lm, const Matrix& rm);

Matrix operator*(const Matrix& lm, const Matrix& rm);


Matrix operator+(const int& a, const Matrix& rm);

Matrix operator-(const int& a, const Matrix& rm);

Matrix operator*(const int& a, const Matrix& rm);

Matrix operator+(const Matrix& lm, const int& a);

Matrix operator-(const Matrix& lm, const int& a);

Matrix operator*(const Matrix& lm, const int& a);

#endif
