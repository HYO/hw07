#include <map>
#include <string>
#include <iostream>
#include "matrix.h"

using namespace std;

istream& operator>>(istream& is, Matrix& m) {
	int r,c;
	is>>r>>c;
	m.Resize(r,c);
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			is>>m(i,j);
		}
	}
	return is;
}

istream& operator>>(istream& is, Vector& v) {
	int r;
	is>>r;
	v.Resize(r);
	for(int i=0; i<r; i++) {
		is>>v[i];
	}
	return is;
}

ostream& operator<<(ostream& os, const Matrix& m) {
	for(int i=0; i<m.rows(); i++) {
		for(int j=0; j<m.cols(); j++) {
			os<<m(i,j)<<" ";
		}
	  os<<endl;
	}
	return os;	
}

Matrix Matrix::operator+() const{
	return *this;
}

Matrix Matrix::operator-() const{
	Matrix ret(*this);
	for(int i=0; i<rows_; i++)
	  for(int j=0; j<cols_; j++)
	    ret(i,j)=(-1)*operator()(i,j);
	return ret;
}

Matrix Matrix::Transpose() const{
  Matrix result;
  result.Resize(cols_,rows_);
  for(int i=0; i<rows_; i++)
    for(int j=0; j<cols_; j++){
      result(j,i)=operator()(i,j);
    }
  return result;
}  

Matrix operator+(const Matrix& lm, const Matrix& rm) {
	Matrix result;
	int r,c;
	r=lm.rows();
	c=lm.cols();
	result.Resize(r,c);
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			result(i,j)=lm(i,j)+rm(i,j);
		}
	}
	return result;
}

Matrix operator-(const Matrix& lm, const Matrix& rm) {
	Matrix result;
	int r,c;
	r=lm.rows();
	c=lm.cols();
	result.Resize(r,c);
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			result(i,j)=lm(i,j)-rm(i,j);
		}
	}
	return result;
}

Matrix operator*(const Matrix& lm, const Matrix& rm) {
	Matrix result;
	int r,c;
	r=lm.rows();
	c=rm.cols();
	result.Resize(r,c);
	for(int i=0; i<r;  i++)
		for(int j=0; j<c; j++) {
			int n=0;
			for(int m=0; m<lm.cols(); m++)
				n+=lm(i,m)*rm(m,j);
			result(i,j)=n;
		}
	return result;
}

Matrix operator+(const int& a, const Matrix& rm) {
		Matrix result;
	int r,c;
	r=rm.rows();
	c=rm.cols();
	result.Resize(r,c);
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			result(i,j)=a+rm(i,j);
		}
	}
	return result;
}

Matrix operator-(const int& a, const Matrix& rm) {
		Matrix result;
	int r,c;
	r=rm.rows();
	c=rm.cols();
	result.Resize(r,c);
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			result(i,j)=a-rm(i,j);
		}
	}
	return result;
}

Matrix operator*(const int& a, const Matrix& rm) {
	Matrix result;
	int r,c;
	r=rm.rows();
	c=rm.cols();
	result.Resize(r,c);
	for(int i=0; i<r;  i++)
		for(int j=0; j<c; j++) {
			result(i,j)=a*rm(i,j);
		}
	return result;
}

Matrix operator+(const Matrix& lm, const int& a) {
		Matrix result;
	int r,c;
	r=lm.rows();
	c=lm.cols();
	result.Resize(r,c);
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			result(i,j)=lm(i,j)+a;
		}
	}
	return result;
}

Matrix operator-(const Matrix& lm, const int& a) {
		Matrix result;
	int r,c;
	r=lm.rows();
	c=lm.cols();
	result.Resize(r,c);
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			result(i,j)=lm(i,j)-a;
		}
	}
	return result;
}

Matrix operator*(const Matrix& lm, const int& a) {
		Matrix result;
	int r,c;
	r=lm.rows();
	c=lm.cols();
	result.Resize(r,c);
	for(int i=0; i<r;  i++)
		for(int j=0; j<c; j++) {
			result(i,j)=a*lm(i,j);
		}
	return result;
}
